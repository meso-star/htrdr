/* Copyright (C) 2018-2019, 2022-2024 Centre National de la Recherche Scientifique
 * Copyright (C) 2020-2022 Institut Mines Télécom Albi-Carmaux
 * Copyright (C) 2022-2024 Institut Pierre-Simon Laplace
 * Copyright (C) 2022-2024 Institut de Physique du Globe de Paris
 * Copyright (C) 2018-2024 |Méso|Star> (contact@meso-star.com)
 * Copyright (C) 2022-2024 Observatoire de Paris
 * Copyright (C) 2022-2024 Université de Reims Champagne-Ardenne
 * Copyright (C) 2022-2024 Université de Versaille Saint-Quentin
 * Copyright (C) 2018-2019, 2022-2024 Université Paul Sabatier
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

#include "atmosphere/htrdr_atmosphere_c.h"
#include "atmosphere/htrdr_atmosphere_ground.h"

#include "core/htrdr.h"
#include "core/htrdr_interface.h"

#include <high_tune/htsky.h>

#include <star/s3d.h>
#include <star/ssf.h>
#include <star/ssp.h>
#include <star/svx.h>

#include <rsys/double2.h>
#include <rsys/double3.h>

enum event {
  EVENT_ABSORPTION,
  EVENT_SCATTERING,
  EVENT_NONE
};

struct filter_context {
  struct ssp_rng* rng;
  const struct htsky* htsky;
  size_t iband; /* Index of the spectral band */
  size_t iquad; /* Index of the quadrature point into the band */

  double Ts; /* Sampled optical thickness */
  double traversal_dst; /* Distance traversed along the ray */

  enum event event_type;
};
static const struct filter_context FILTER_CONTEXT_NULL = {
  NULL, NULL, 0, 0, 0.0, 0.0, EVENT_NONE
};

/*******************************************************************************
 * Helper functions
 ******************************************************************************/
static int
hit_filter
  (const struct svx_hit* hit,
   const double org[3],
   const double dir[3],
   const double range[2],
   void* context)
{
  struct filter_context* ctx = context;
  double kext_max;
  int pursue_traversal = 1;
  ASSERT(hit && ctx && !SVX_HIT_NONE(hit) && org && dir && range);
  (void)range;

  kext_max = htsky_fetch_svx_voxel_property(ctx->htsky, HTSKY_Kext,
    HTSKY_SVX_MAX, HTSKY_CPNT_MASK_ALL, ctx->iband, ctx->iquad, &hit->voxel);

  ctx->traversal_dst = hit->distance[0];

  for(;;) {
    double vox_dst = hit->distance[1] - ctx->traversal_dst;
    const double T = vox_dst * kext_max;

    /* A collision occurs behind `vox_dst' */
    if(ctx->Ts > T) {
      ctx->Ts -= T;
      ctx->traversal_dst = hit->distance[1];
      pursue_traversal = 1;
      break;

    /* A real/null collision occurs before `vox_dst' */
    } else {
      const double collision_dst = ctx->Ts / kext_max;
      double pos[3];
      double ks, ka;
      double r;
      double proba_abs;
      double proba_sca;

      /* Compute the traversed distance up to the challenged collision */
      ctx->traversal_dst += collision_dst;
      ASSERT(ctx->traversal_dst >= hit->distance[0]);
      ASSERT(ctx->traversal_dst <= hit->distance[1]);

      /* Compute the world space position where a collision may occur */
      pos[0] = org[0] + ctx->traversal_dst * dir[0];
      pos[1] = org[1] + ctx->traversal_dst * dir[1];
      pos[2] = org[2] + ctx->traversal_dst * dir[2];

      ka = htsky_fetch_raw_property(ctx->htsky, HTSKY_Ka, HTSKY_CPNT_MASK_ALL,
        ctx->iband, ctx->iquad, pos, -DBL_MAX, DBL_MAX);
      ks = htsky_fetch_raw_property(ctx->htsky, HTSKY_Ks, HTSKY_CPNT_MASK_ALL,
        ctx->iband, ctx->iquad, pos, -DBL_MAX, DBL_MAX);

      r = ssp_rng_canonical(ctx->rng);
      proba_abs = ka / kext_max;
      proba_sca = ks / kext_max;
      if(r < proba_abs) { /* Absorption event */
        pursue_traversal = 0;
        ctx->event_type = EVENT_ABSORPTION;
        break;
      } else if(r < proba_abs + proba_sca) { /* Scattering event */
        pursue_traversal = 0;
        ctx->event_type = EVENT_SCATTERING;
        break;
      } else { /* Null collision */
        ctx->Ts = ssp_ran_exp(ctx->rng, 1); /* Sample a  new optical thickness */
      }
    }
  }
  return pursue_traversal;
}

/*******************************************************************************
 * Local functions
 ******************************************************************************/
double
atmosphere_compute_radiance_lw
  (struct htrdr_atmosphere* cmd,
   const size_t ithread,
   struct ssp_rng* rng,
   const double pos_in[3],
   const double dir_in[3],
   const double wlen, /* In nanometer */
   const size_t iband,
   const size_t iquad)
{
  struct s3d_hit s3d_hit = S3D_HIT_NULL;
  struct s3d_hit s3d_hit_prev = S3D_HIT_NULL;
  struct svx_hit svx_hit = SVX_HIT_NULL;
  struct ssf_phase* phase_hg = NULL;

  double wo[3];
  double pos[3];
  double dir[3];
  double range[2];
  double pos_next[3];
  double dir_next[3];
  double temperature;
  double wlen_m = wlen * 1.e-9;
  double g;
  double w = 0; /* Weight */

  ASSERT(cmd && rng && pos_in && dir_in);
  ASSERT(ithread < htrdr_get_threads_count(cmd->htrdr));

  /* Setup the phase function for this spectral band & quadrature point */
  CHK(RES_OK == ssf_phase_create
    (htrdr_get_thread_allocator(cmd->htrdr, ithread),
     &ssf_phase_hg,
     &phase_hg));
  g = htsky_fetch_per_wavelength_particle_phase_function_asymmetry_parameter
    (cmd->sky, wlen);
  SSF(phase_hg_setup(phase_hg, g));

  /* Initialise the random walk */
  d3_set(pos, pos_in);
  d3_set(dir, dir_in);
  d2(range, 0, INF);

  for(;;) {
    struct filter_context ctx = FILTER_CONTEXT_NULL;

    /* Find the first intersection with the surface geometry */
    d2(range, 0, DBL_MAX);
    HTRDR(atmosphere_ground_trace_ray
      (cmd->ground, pos, dir, range, &s3d_hit_prev, &s3d_hit));

    /* Sample an optical thickness */
    ctx.Ts = ssp_ran_exp(rng, 1);

    /* Setup the remaining fields of the hit filter context */
    ctx.rng = rng;
    ctx.htsky = cmd->sky;
    ctx.iband = iband;
    ctx.iquad = iquad;

    /* Fit the ray range to the surface distance along the ray */
    d2(range, 0, s3d_hit.distance);

    /* Trace a ray into the participating media */
    HTSKY(trace_ray(cmd->sky, pos, dir, range, NULL,
      hit_filter, &ctx, iband, iquad, &svx_hit));

    /* No scattering and no surface reflection.
     * Congratulation !! You are in space. */
    if(S3D_HIT_NONE(&s3d_hit) && SVX_HIT_NONE(&svx_hit)) {
      w = 0;
      break;
    }

    /* Compute the next position */
    pos_next[0] = pos[0] + dir[0]*ctx.traversal_dst;
    pos_next[1] = pos[1] + dir[1]*ctx.traversal_dst;
    pos_next[2] = pos[2] + dir[2]*ctx.traversal_dst;

    /* Absorption event. Stop the realisation */
    if(ctx.event_type == EVENT_ABSORPTION) {
      ASSERT(!SVX_HIT_NONE(&svx_hit));
      temperature = htsky_fetch_temperature(cmd->sky, pos_next);
      /* weight is planck integrated over the spectral sub-interval */
      w = htrdr_planck_monochromatic(wlen_m, temperature);
      break;
    }

    /* Negate the incoming dir to match the convention of the SSF library */
    d3_minus(wo, dir);

    /* Scattering in the volume */
    if(ctx.event_type == EVENT_SCATTERING) {
      ASSERT(!SVX_HIT_NONE(&svx_hit));
      ssf_phase_sample(phase_hg, rng, wo, dir_next, NULL);
      s3d_hit_prev = S3D_HIT_NULL;

    /* Scattering at a surface */
    } else {
      struct htrdr_interface interf = HTRDR_INTERFACE_NULL;
      const struct htrdr_mtl* mtl = NULL;
      struct ssf_bsdf* bsdf = NULL;
      double bounce_reflectivity = 0;
      double N[3];
      int type;
      ASSERT(ctx.event_type == EVENT_NONE);
      ASSERT(!S3D_HIT_NONE(&s3d_hit));

      /* Fetch the hit interface materal and build its BSDF */
      htrdr_atmosphere_ground_get_interface(cmd->ground, &s3d_hit, &interf);
      mtl = htrdr_interface_fetch_hit_mtl(&interf, dir, &s3d_hit);
      HTRDR(mtl_create_bsdf(cmd->htrdr, mtl, ithread, wlen, rng, &bsdf));

      d3_normalize(N, d3_set_f3(N, s3d_hit.normal));
      if(d3_dot(N, wo) < 0) d3_minus(N, N);

      bounce_reflectivity = ssf_bsdf_sample
        (bsdf, rng, wo, N, dir_next, &type, NULL);
      if(!(type & SSF_REFLECTION)) { /* Handle only reflections */
        bounce_reflectivity = 0;
      }

      /* Release the BSDF */
      SSF(bsdf_ref_put(bsdf));

      if(ssp_rng_canonical(rng) >= bounce_reflectivity) { /* Absorbed at boundary */
        temperature = mtl->temperature; /* Fetch mtl temperature */
        /* Weight is planck integrated over the spectral sub-interval */
        w = temperature>0 ? htrdr_planck_monochromatic(wlen_m, temperature) : 0;
        break;
      }
      s3d_hit_prev = s3d_hit;
    }
    d3_set(pos, pos_next);
    d3_set(dir, dir_next);
  }
  SSF(phase_ref_put(phase_hg));
  return w;
}

